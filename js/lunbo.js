//轮播图脚本如下：

var Speed = 1; //速度(毫秒)
var Space = 5; //每次移动(px)
var PageWidth = 333; //翻页宽度
var fill = 0; //整体移位
var MoveLock = false;
var MoveTimeObj;
var Comp = 0;
var AutoPlayObj = null;

GetObj("List2").innerHTML = GetObj("List1").innerHTML;
GetObj('Cont').scrollLeft = fill;
GetObj("Cont").onmouseover = function(){clearInterval(AutoPlayObj);}
GetObj("Cont").onmouseout = function(){AutoPlay();}
AutoPlay();

function GetObj(objName){
	if(document.getElementById){
		return eval('document.getElementById("'+objName+'")')
	}
	else{
		return eval('document.all.'+objName)
	}
}

function AutoPlay(){ //自动滚动
 clearInterval(AutoPlayObj);
 AutoPlayObj = setInterval('GoDown();StopDown();',3000); //间隔时间
}

function GoUp(){ //上翻开始
	if(MoveLock) return;
	clearInterval(AutoPlayObj);
	MoveLock = true;
	MoveTimeObj = setInterval('ScrUp();',Speed);
}

function StopUp(){ //上翻停止
	clearInterval(MoveTimeObj);
	if(GetObj('Cont').scrollLeft % PageWidth - fill != 0){
	 	Comp = fill - (GetObj('Cont').scrollLeft % PageWidth);
	 	CompScr();
	}
	else{
		MoveLock = false;
	}
 	AutoPlay();
}

function ScrUp(){ //上翻动作
	if(GetObj('Cont').scrollLeft <= 0){
		GetObj('Cont').scrollLeft = GetObj('Cont').scrollLeft + GetObj('List1').offsetWidth
	}
	GetObj('Cont').scrollLeft -= Space ;
}

function GoDown(){ //下翻
	clearInterval(MoveTimeObj);
	if(MoveLock) return;
	clearInterval(AutoPlayObj);
	MoveLock = true;
	ScrDown();
	MoveTimeObj = setInterval('ScrDown()',Speed);
}

function StopDown(){ //下翻停止
	clearInterval(MoveTimeObj);
	if(GetObj('Cont').scrollLeft % PageWidth - fill != 0 ){
	Comp = PageWidth - GetObj('Cont').scrollLeft % PageWidth + fill;
	CompScr();
}
else{
	MoveLock = false;
}
AutoPlay();
}

function ScrDown(){ //下翻动作
	if(GetObj('Cont').scrollLeft >= GetObj('List1').scrollWidth){
		GetObj('Cont').scrollLeft = GetObj('Cont').scrollLeft - GetObj('List1').scrollWidth;
	}
	GetObj('Cont').scrollLeft += Space ;
}

function CompScr(){
	var num;
	if(Comp == 0){
	 	MoveLock = false;return;
	}
 
	if(Comp < 0){ //上翻
		if(Comp < -Space){
			Comp += Space;
			num = Space;
		}
		else{
			num = -Comp;
			Comp = 0;
		}
		GetObj('Cont').scrollLeft -= num;
		setTimeout('CompScr()',Speed);
		}
 
		else{ //下翻
		  if(Comp > Space){
		   Comp -= Space;
		   num = Space;
		}
		  else{
		   num = Comp;
		   Comp = 0;
		}
		GetObj('Cont').scrollLeft += num;
		setTimeout('CompScr()',Speed);
	}
}