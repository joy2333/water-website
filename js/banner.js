$(document).ready(function(){
	//全屏切换
	var img_h=$("html,body").width()?Math.ceil($("html,body").width()*500/1920):auto;
	var box=$("#banner");
	box.height(img_h);
	//var imgs=$("#banner .img_list ul li img");	
	 
	$(window).on("resize",function(){
		box.height(Math.ceil($("html,body").width()*500/1920));
		 
	});
		
	var page =1;
	var img_list = $("#banner .img_list ul li");
	var img_total = img_list.length;
	var small_nav = $("#banner .small_nav li");
	
	 function slider_animate(){
	        //切换图片
	        img_list.eq(page-1).animate({"opacity":1,"z-index":1},100);   
	        img_list.eq(page-1).siblings().animate({"opacity":0,"z-index":0},100); 
	        small_nav.eq(page-1).addClass('active'); 
	        small_nav.eq(page-1).siblings().removeClass('active'); 
	}
	  //右边按钮
    $(".next_btn a").bind("click",function(e){
        e.preventDefault();//阻止a 的默认事件/行为
        page++;

        if(page>img_total){
            page=1;//回到第一个
        }
        //切换,小圆点一起动
        slider_animate();
    });

    //左边按钮
    $(".pre_btn a").bind("click",function(e){
        e.preventDefault();//阻止a 的默认事件/行为
        page--;
        
        //已经到第1个图片
        if(page<1){
            page=img_total;//回到最后一个
        }
        //当前图片 透明度为1，z-index 为1，其它的图片所在的li 透明度为0，z-index 为0。
        slider_animate();
    });

	//小导航切换功能
    small_nav.on("click",function(e){
    	e.preventDefault();
        page = $(this).attr("data-index"); 
        slider_animate();
    });

    //自动切换:自动向后切换
    var timer = setInterval(function(){
        page++;
        if(page>img_total){
            page=1; 
        }
        
        slider_animate();
    },5000);

 	$("#banner").hover(
        function(){
            clearInterval(timer); 
        },
        function(){
            
            timer = setInterval(function(){
                page++;
                if(page>img_total){
                    page=1; 
                }
                 
                slider_animate();
            },5000);
        }
    );
 	$("#banner a.leftbtn").click(function(){
 		 page--;
        if(page<1){
            page=img_total; 
        }
        
        slider_animate();
 		
 		
 	});
 		
 	$("#banner a.rightbtn").click(function(){
 		 page++;
        if(page>img_total){
            page=1; 
        }
        
        
        slider_animate();
 		
 		
 	});	
 	

});
