/*alert(1);*/
$(document).ready(function(){
	/*alert(1);*/
	var page=1;
	var img_list_ul=$(".prolist ul");
	var img_list = $(".prolist ul li"); //图片列表
    var img_total = img_list.length;//获得图片总数

	//自定义函数：实现动画，小圆点一起动的效果
    function slider_animate(){
        //切换图片
        img_list_ul.animate({"left":-(page-1)*333+"px"},500);
    }

	$(".content .left").bind("click",function(e){
        e.preventDefault();//阻止a 的默认事件/行为
        page--;
        //已经到第1个图片
        if(page<1){
            page=img_total-2;//回到最后一个
        }
        slider_animate()
    });

    $(".content .right").bind("click",function(e){
        e.preventDefault();//阻止a 的默认事件/行为
        page++;
        //已经到第1个图片
        if(page>img_total-2){
            page=1;//回到最后一个
        }
        slider_animate()
    });



});